export const PATHS = {
  RESOURCES: 'resources/',
};

export const ENDPOINTS = {
  BOOK: 'BookStore/v1/Book',
  BOOKS: 'BookStore/v1/Books',
  LOGIN: 'Account/v1/Login',
  USER: 'Account/v1/User',
};
